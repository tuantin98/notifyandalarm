package com.aspei.notificationdatabase;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import com.aspei.notificationdatabase.Database.TaskDatabase;
import com.aspei.notificationdatabase.Database.TaskItem;
import com.aspei.notificationdatabase.Notification.AlarmReceiver;
import com.aspei.notificationdatabase.Notification.NotificationReceiver;
import com.aspei.notificationdatabase.Notification.PriorityDialogFragment;
import com.aspei.notificationdatabase.Notification.ReminderTypeDialogFragment;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

// Implements Interface of reminder type dialog
public class MainActivity extends AppCompatActivity
        implements ReminderTypeDialogFragment.IReminderTypeDialogListener, PriorityDialogFragment.IPriorityDialogListener {
    private String TAG = "Show_project";

    //widgets
    private DialogFragment dialogFragment;

    Button btnShow,btnInsert,btnDelete;
    TextView txtDateTime;
    EditText edtId,edtName,edtNotes,edtReminder,edtRepeat;

    TextView mTxtReminderType;  // text to view reminder type
    Button btnShowReminderType;  // button show dialog to pick reminder type
    TextView mTxtPriority;
    Button btnShowPriority;
    // vars
    int mYear,mDate,mMonth,mHour,mMinute;
    private String reminderType = "None";
    private String priority = "Low";
    Calendar calendar;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Mapping();
        calendar = Calendar.getInstance();
        final AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
                txtDateTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PickDate();
            }
        });

        // Trigger event show dialog and pick reminder type
        mTxtReminderType = findViewById(R.id.text_reminder_type);
        btnShowReminderType = findViewById(R.id.btn_show_reminder_type);
        btnShowReminderType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showReminderTypeDialog();
            }
        });


        btnShowPriority.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPriorityDialog();
            }
        });




        btnInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = edtName.getText().toString();
                String notes= edtNotes.getText().toString();
                long time = getTime(mYear,mMonth,mDate,mHour,mMinute);
                int reqCode = Integer.parseInt( edtId.getText().toString() );
                // intent action alarm/notification
                Log.d(TAG,"reminder type : "+ reminderType);
                switch (reminderType) {
                    case "None" :
                    break;
                    case "Notification":
                        Intent intent = new Intent(MainActivity.this, NotificationReceiver.class);
                        intent.putExtra("ID", edtId.getText().toString()  );
                        intent.putExtra("NAME", name);
                        intent.putExtra("NOTES", notes);
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(MainActivity.this,reqCode,intent,0);
                        alarmManager.setExact(AlarmManager.RTC_WAKEUP,time, pendingIntent);
                        break;
                    case "Alarm":
                        Intent intent1 = new Intent(MainActivity.this, AlarmReceiver.class);
                        PendingIntent pendingIntent1 = PendingIntent.getBroadcast(MainActivity.this,reqCode,intent1,0);
                        alarmManager.setExact(AlarmManager.RTC_WAKEUP,time, pendingIntent1);
                        break;
                }

                Log.d(TAG, "name: "+name+", notes: "+notes+", time: "+time);
                // TaskDatabase.getInstance(mContext).insert(new TaskItem(name, notes, time));
                //Toast.makeText(MainActivity.this,"Insert success", Toast.LENGTH_SHORT).show();
            }
        });
        btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<TaskItem> arrayTask = TaskDatabase.getInstance(mContext).getTaskList();
                showAllColumns(arrayTask);
            }
        });
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id = Integer.parseInt( edtId.getText().toString() );
                AlarmManager alarmManager1 = (AlarmManager)getSystemService(ALARM_SERVICE);
                Intent intent = new Intent(MainActivity.this, NotificationReceiver.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(MainActivity.this, id, intent,PendingIntent.FLAG_CANCEL_CURRENT );
                alarmManager1.cancel(pendingIntent);
                //Toast.makeText(MainActivity.this, "Notify is deleted", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void Mapping() {
        btnShow = findViewById(R.id.btnShow);
        btnInsert = findViewById(R.id.btn_insert);
        btnShow = findViewById(R.id.btnShow);
        btnDelete = findViewById(R.id.btn_delete);

        edtId = findViewById(R.id.edt_id);
        edtName = findViewById(R.id.edt_name);
        edtNotes = findViewById(R.id.edt_notes);

        txtDateTime = findViewById(R.id.txtDateTime);

        mTxtPriority = findViewById(R.id.txt_priority);
        btnShowPriority = findViewById(R.id.btn_show_priority);
    }
    private void PickDate(){
        final Calendar calendar = Calendar.getInstance();
        int year1 = calendar.get(Calendar.YEAR);
        int month1 = calendar.get(Calendar.MONTH);
        int date1 = calendar.get(Calendar.DATE);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int date) {
                mYear = year;
                mMonth = month;
                mDate = date;
                PickTime();
                calendar.set(year,month,date);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                txtDateTime.setText(simpleDateFormat.format(calendar.getTime())+" || ");

            }
        },year1,month1,date1);
        datePickerDialog.show();
    }
    private void PickTime(){
       final Calendar calendar = Calendar.getInstance();
        int hour1  = calendar.get(Calendar.HOUR_OF_DAY);
        int minute1 = calendar.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                mHour = hour;
                mMinute = minute;
                calendar.set(0,0,0,hour,minute,0);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
                txtDateTime.append(simpleDateFormat.format(calendar.getTime()));
            }
        },hour1, minute1,true);
        timePickerDialog.show();
    }
    private long getTime(int year, int month, int date, int hour, int minute){
        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(Calendar.YEAR, year);
        calendar1.set(Calendar.MONTH,month);
        calendar1.set(Calendar.DAY_OF_MONTH,date);
        calendar1.set(Calendar.HOUR_OF_DAY,hour);
        calendar1.set(Calendar.MINUTE,minute);
        calendar1.set(Calendar.SECOND,0);
        return calendar1.getTimeInMillis();
    }
    private void showAllColumns(ArrayList <TaskItem> arrayList){
        for ( TaskItem taskItem : arrayList){
            Log.d(TAG, taskItem.toString());
        }
    }


    // function to show dialog reminder types
    private void showReminderType() {
        mTxtReminderType.setText(reminderType);
    }

    private void showPriority() {
        mTxtPriority.setText(priority);
    }

    @Override
    public void sendInput(String input) {
        Log.d(TAG, "sendInput: got the input: " + input);
        reminderType = input;
        showReminderType();
    }

    @Override
    public void sendPriority(String input) {
        priority = input;
        showPriority();
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) { }

    private void showReminderTypeDialog() {
        dialogFragment = new ReminderTypeDialogFragment();
        dialogFragment.show(getSupportFragmentManager(), "ReminderTypeDialogFragment");
    }

    private void showPriorityDialog() {
        dialogFragment = new PriorityDialogFragment();
        dialogFragment.show(getSupportFragmentManager(), "PriorityDialogFragment");
    }

}
