package com.aspei.notificationdatabase.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MySQliteOpenHelper extends SQLiteOpenHelper {
    private static final String  DATABASE_NAME = "task.db";
    private static final Integer DATABASE_VERSION = 1;
    public static final String  TASK_TABLE = "task_table";
    public static final String  ID = "id";
    public static final String  NAME = "name";
    public static final String  NOTES = "notes";
    public static final String  DUEDATE = "duedate";
    MySQliteOpenHelper(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_SQL = String.format("CREATE TABLE %s ("+
                "%s INTEGER PRIMARY KEY AUTOINCREMENT,"+
                "%s TEXT,"+
                "%s TEXT,"+
                "%s LONG)",
        TASK_TABLE,ID,NAME,NOTES,DUEDATE);
        sqLiteDatabase.execSQL(CREATE_SQL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        String DROP_SQL = String.format("DROP TABLE IF EXIST %s",TASK_TABLE);
        sqLiteDatabase.execSQL(DROP_SQL);
    }
    public String[] getAllColumns(){
        return new String[] { ID,NAME,NOTES,DUEDATE } ;
    }
}
