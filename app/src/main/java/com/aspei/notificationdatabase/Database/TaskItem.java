package com.aspei.notificationdatabase.Database;

public class TaskItem {
    private int id;
    private String name;
    private String notes;
    private long dueDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public long getDueDate() {
        return dueDate;
    }

    public void setDueDate(long dueDate) {
        this.dueDate = dueDate;
    }

    public TaskItem(int id, String name, String notes, long dueDate) {
        this.id = id;
        this.name = name;
        this.notes = notes;
        this.dueDate = dueDate;
    }

    public TaskItem(String name, String notes, long dueDate) {
        this.name = name;
        this.notes = notes;
        this.dueDate = dueDate;
    }

    @Override
    public String toString() {
        return "TaskItem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", notes='" + notes + '\'' +
                ", dueDate=" + dueDate +
                '}';
    }
}
