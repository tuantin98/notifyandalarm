package com.aspei.notificationdatabase.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import java.util.ArrayList;

public class TaskDatabase {
    private MySQliteOpenHelper mySQliteOpenHelper;

    private TaskDatabase(Context context){
        mySQliteOpenHelper = new MySQliteOpenHelper(context);
    }
    private static TaskDatabase instance;
    public static TaskDatabase getInstance(Context context){
        if (instance == null )
            synchronized (TaskDatabase.class){
                if (instance == null){
                    instance = new TaskDatabase(context);
                }
            }
        return instance;
    }
    public void insert(TaskItem taskItem){
        SQLiteDatabase sqLiteDatabase = instance.mySQliteOpenHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(MySQliteOpenHelper.NAME, taskItem.getName());
        contentValues.put(MySQliteOpenHelper.NOTES, taskItem.getNotes());
        contentValues.put(MySQliteOpenHelper.DUEDATE,taskItem.getDueDate());

        sqLiteDatabase.insert(MySQliteOpenHelper.TASK_TABLE,null, contentValues);
        sqLiteDatabase.close();
    }
    public ArrayList < TaskItem > getTaskList(){
        ArrayList <TaskItem> arrTask = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = instance.mySQliteOpenHelper.getReadableDatabase();
        String[] columns = mySQliteOpenHelper.getAllColumns();
        Cursor cursor = sqLiteDatabase.query(MySQliteOpenHelper.TASK_TABLE, columns,
                null,null,null,null,null);
        while (cursor.moveToNext()){
            int id  = cursor.getInt(0);
            String name = cursor.getString(1);
            String notes = cursor.getString(2);
            long time = cursor.getLong(3);
            arrTask.add(new TaskItem(id,name,notes,time));
        }
        cursor.close();
        return arrTask;
    }
}
